/*
 * (c) 2017 Benjamin Bieler <ben@benbieler.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

package de.bbieler.seminararbeit.controller;

import de.bbieler.seminararbeit.model.ImageManipulatorModel;
import de.bbieler.seminararbeit.view.ImageManipulatorView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * main.java.de.bbieler.seminararbeit.controller.ImageManipulatorController
 *
 * @author benbieler <15/04/2017>.
 */
public class ImageManipulatorController {
    private ImageManipulatorView imageManipulatorView;
    private ImageManipulatorModel imageManipulatorModel;

    public ImageManipulatorController(ImageManipulatorModel imageManipulatorModel, ImageManipulatorView imageManipulatorView) {

        this.imageManipulatorModel = imageManipulatorModel;
        this.imageManipulatorView = imageManipulatorView;

        this.imageManipulatorView.addCalculationListener(new CalculationListener());
        this.imageManipulatorView.addClearListener(new ClearListener());
    }

    class CalculationListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            int firstNumber, secondNumber, thirdNumber, scalingFactor;
            String interpolationType;

            try {
                firstNumber = imageManipulatorView.getFirstGS();
                secondNumber = imageManipulatorView.getSecondGS();
                thirdNumber = imageManipulatorView.getThirdGS();
                scalingFactor = imageManipulatorView.getScalingFactor();

                if (firstNumber > 255 || secondNumber > 255 || thirdNumber > 255) {
                    imageManipulatorView.displayErrorMessage("Please enter a valid Grey scale value (0 to 255)");
                } else if (scalingFactor > 10) {
                    imageManipulatorView.displayErrorMessage("Please enter a scaling factor from 0 to 10");
                } else {

                        interpolationType = imageManipulatorView.getInterpolationType();

                        if (interpolationType.equals("Nearest Neighbor Interpolation")) {
                            imageManipulatorView.clearTable();
                            imageManipulatorView.setNNICalculationResult(imageManipulatorModel.handleNNICalculation(firstNumber, secondNumber, thirdNumber, scalingFactor));
                        }

                        if (interpolationType.equals("Linear Interpolation")) {
                            imageManipulatorView.clearTable();
                            imageManipulatorView.setLICalculationResult(imageManipulatorModel.handleLICalculation(firstNumber, secondNumber, thirdNumber, scalingFactor));
                        }

                        if (interpolationType.equals("Cubic Spline Interpolation")) {
                            imageManipulatorView.clearTable();
                            imageManipulatorView.setCUCalculationResult(imageManipulatorModel.handleCUCalculation(firstNumber, secondNumber, thirdNumber, scalingFactor));
                        }
                    }

            } catch (NumberFormatException ex) {
                imageManipulatorView.displayErrorMessage("You have to enter three values of type integer");
            }
        }
    }

    class ClearListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            imageManipulatorView.clearTable();
        }
    }
}
